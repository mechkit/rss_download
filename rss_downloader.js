#!/usr/bin/env node

const async = require('async');
const FeedParser = require('feedparser');
const fs = require('fs');
const readline = require('readline');
const fetch = require('node-fetch'); // for fetching the feed
const youtubedl = require('@microlink/youtube-dl');

var program = require('commander');
program.version('0.2.0');
program
  .option('-d, --dir <base_path>', 'Target directory')
  .option('-u, --urls <urls_path>', 'url file path')
  .option('-o, --output <output_path>', 'output directory')
  .parse(process.argv);
//program.parse(process.argv);
//const base_path = program.base_path || __dirname+'/base';
const base_path = process.argv[2] || __dirname+'/base';
const urls_path = program.urls_path || base_path + '/urls';
const output_path = program.output_path || base_path + '/output';
const feed_file_path = program.output_path || base_path + '/feeds';

var log_file = fs.createWriteStream(base_path+'/'+'rss_image_downloader.log', {flags:'a'});
var log = function(){
  let line = Array.from(arguments).join(' ');
  console.log(line);
  log_file.write( line + '\n');
  //fs.writeFileSync(base_path+'/'+'log.txt', Array.from(arguments).join(' '), {flag:'a'});
};

if ( !fs.existsSync(output_path) ){
  fs.mkdirSync(output_path);
}
if ( !fs.existsSync(feed_file_path) ){
  fs.mkdirSync(feed_file_path);
}

var urls = fs.readFileSync(urls_path, 'utf8').split('\n');


var history_set = new Set();
var history_file;

if (!fs.existsSync(base_path+'/'+'history.log')){
  fs.writeFileSync(base_path+'/'+'history.log', '' );
}
history_file = fs.createReadStream(base_path+'/'+'history.log');
const history_file_stream = readline.createInterface({
  input: fs.createReadStream( base_path+'/'+'history.log' ),
  crlfDelay: Infinity
});
history_file_stream.on('error', (error) => {
  log('Error: ', error);
});
history_file_stream.on('line', (line) => {
  history_set.add(line);
});


//   .option('-r, --recursive', 'Remove recursively')
//   .action(function (dir, cmd) {
//     log('remove ' + dir + (cmd.recursive ? ' recursively' : ''));
//   });
//
// program
//   .command('report <dir>')
//   .option('-r, --recursive', 'Remove recursively')
//   .action(function (dir, cmd) {
//
//     log('tp ' + dir + (cmd.recursive ? ' recursively' : ''));
//   });
//
// program.parse(process.argv);

var download_image = function(url, file_path, callback){
  log('...starting:',url);

  fetch(url)
  .then( res => {
    if (res.ok) {
      log(res.statusText, url); // 200
      log('headers', res.headers);
      res.body.pipe(fs.createWriteStream(file_path))
      .on('finish', () => {
        console.log('The file is finished downloading.');
        log(file_path);
        history_file.write( url + '\n');
        setTimeout(callback, 50);
      })
      .on('error', function(err) {
        log('X image request error: ', err);
        history_file.write( url + '\n');
        setTimeout(callback, 50);
      });
    } else {
      log('X Bad status code: ' + url +' '+ res.statusText);
      callback();
    }
  })
  .catch( error => {
    log('X image request error: ', error);
    history_file.write( url + '\n');
    setTimeout(callback, 50);
  });

};

var download_video = function(url, file_path, callback){
  log('...starting:',url);
  var video = youtubedl(url,
    // Optional arguments passed to youtube-dl.
    [],
    // Additional options can be given for calling `child_process.execFile()`.
    // { cwd: __dirname+base_path });
    { cwd: output_path }
  );

  // Will be called when the download starts.
  video.on('info', function(info) {
    log('Download started');
    log('starting filename: ' + info._filename);
    log('size: ' + info.size);
  });

  video.pipe(fs.createWriteStream(file_path));

  video.on('error', function error(err) {
    log('error 2:', err);
    callback();
  });

  // Will be called if download was already completed and there is nothing more to download.
  video.on('complete', function complete(info) {
    'use strict';
    console.log('filename: ' + info._filename + ' already downloaded.');
    history_file.write( url + '\n');
    log('_____________________________/');
    callback();
  });

  video.on('end', function() {
    log('finished downloading', url);
    history_file.write( url + '\n');
    callback();
  });
};



function download_rss(feed_url, callback){
  var feed_name = feed_url.replace(/http:\/\/|https:\/\//g,'').replace(/[:/]/g,'_');
  log('-FEED- '+feed_name);
  var feedparser = new FeedParser();
  fetch(feed_url)
  .then( res => {
    if (res.ok) {
      res.body.pipe(fs.createWriteStream( feed_file_path +'/'+ feed_name ));
      res.body.pipe(feedparser);
    } else {
      log('X Bad status code: ' + feed_name +' '+ res.statusText);
      callback();
    }
  })
  .catch( error => {
    log('req error:', error, feed_url);
    callback();
  });

  feedparser.on('error', function (error) {
    log('feedparser error:', error, feed_url);
    //callback();
  });

  feedparser.on('readable', function () {
    // This is where the action is!
    var stream = this; // `this` is `feedparser`, which is a stream
    var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
    var item;

    while( item = stream.read()) {
      // log(item);
      // var regex_image_url = /"http[^"]*\.jpg|"http[^"]*\.png|"http[^"]*\.gif/g;
      var regex_image_url = /http[^"]*png|http[^"]*jpg|http[^"]*jpeg|http[^"]*\.gif|http[^"]*\.gifv|https:\/\/gfycat[^"]*/g;
      var summary_urls = ( item.summary && item.summary.match(regex_image_url) ) || [];
      var description_urls = ( item.description && item.description.match(regex_image_url) ) || [];
      var urls = summary_urls.concat(description_urls);
      var entry = item.link.replace(/http:\/\/|https:\/\//g,'').split('/').slice(1).join('_');
      if(urls){
        for( var url of urls){
          // url = url.slice(9);
          var skip = false;
          var type = 'image';

          ///////////
          // Mixed URL fixes
          url = url.replace(/\s.*$/,'');
          url = url.replace(/\?.*$/,'');
          url = url.replace(/-[0-9]+x[0-9]+\./,'');
          // tumblr
          if( url.indexOf('tumblr.com') !== -1 ){
            if( url.indexOf('_500.') !== -1 ){
              url = url.replace(/_500\./,'_1280.');
            }
            if( url.indexOf('_400.') !== -1 ){
              url = url.replace(/_400\./,'_1280.');
            }
          }
          url = encodeURI(url);
          ////////

          skip = skip || history_set.has(url);
          skip = skip || to_download_set.has(url);
          skip = skip || url.indexOf('.thumbs.') !== -1 ; // Reddit
          if( ! skip ){

            to_download_set.add(url);
            var image_name = url.split('/').slice(-1)[0];
            // Fix gfycat
            if( url.indexOf('https://gfycat.com') !== -1 ){
              //log(url);
              var image_url_parts = url.split('?');
              // url = image_url_parts[0] + '.gif';
              image_name = image_name.split('?')[0];
              type = 'video';
              //log(url);
              //log(image_name);
            }
            let filename = feed_name+'-'+entry+'-'+image_name;
            filename = filename.replace(/[^a-z0-9._-]/gi, '_').replace(/_{2,}/g, '_');
            let file_path = output_path +'/'+filename;
            to_download_array.push({
              type,
              url,
              filename,
              file_path,
            });
          } else {
            // log('Rejecting: '+url);
          }
        }
      }
    }
  });

  feedparser.on('end', function (something) {
    log('feedparser done:', something, feed_url);
    callback();
  });
}




var download_queue = async.queue(function(task, callback) {
  if( task.type === 'image' ){
    download_image(task.url, task.file_path, callback);
  }
  if( task.type === 'video' ){
    //download_video(task.url, task.file_path, callback);
  }
}, 4);

download_queue.drain(function() {
  log('#############################');
  log('all items have been processed');
  log('#############################');
});

download_queue.error(function(err, task) {
  log('XXX task experienced an error');
});

var rss_queue = async.queue(function(task, callback) {
  let feed_url = task;
  download_rss(feed_url, callback);
}, 8);

rss_queue.drain(function() {
  log('##################');
  log('RSS queue done');
  log('to download:', to_download_set.size);
  log('##################');
  download_queue.push( Array.from(to_download_array) );
  //fs.writeFileSync(base_path+'/'+'history.json', JSON.stringify([...history_set]));
  //log_file.end();
});

rss_queue.error(function(err, task) {
  console.error('XXX task experienced an error');
});


var to_download_array = [];
var to_download_set = new Set();
var rss_set = new Set();

history_file_stream.on('close', () => {
  history_file = fs.createWriteStream(base_path+'/'+'history.log', {flags:'a'});
  urls.forEach(url => {
    if(url && url[0] !== '/' && url[0] !== '#' ){
      rss_set.add(url);
    }
  });
  log('rss_queue',rss_set.size);
  rss_queue.push( Array.from(rss_set) );
});

